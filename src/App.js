import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <section>
        <h1>Form Pembelian Buah</h1>
        <form>
          <table width="450px">
            <tr>
              <td>
                <label for="username">Nama Pelanggan</label>
              </td>
              <td>
                <input type="text" id="username" />
              </td>
            </tr>
            <br />

            <tr>
              <td className="daftarItem">
                <label for="">Daftar Item</label>
              </td>
              <td>
                <input type="checkbox" id="Semangka" /><label for="Semangka" >Semangka</label><br />
                <input type="checkbox" id="Jeruk" /><label for="Jeruk">Jeruk</label><br />
                <input type="checkbox" id="Nanas" /><label for="Nanas">Nanas</label><br />
                <input type="checkbox" id="Salak" /><label for="Salak">Salak</label><br />
                <input type="checkbox" id="Anggur" /><label for="Anggur">Anggur</label>
              </td>
            </tr>
            <tr>
              <td>
                <input type="submit" value="Kirim" />
              </td>
            </tr>
          </table>
        </form>

      </section>
    </div>
  );
}

export default App;
